package com.sem;

import com.sem.analysis.LiveVariablesInstrumenter;
import com.sem.analysis.ReachingDefinitionsInstrumenter;
import com.sem.analysis.VeryBusyExpressionInstrumenter;
import soot.Pack;
import soot.PackManager;
import soot.Transform;

public class Main
{
	public static void main(String[] args)
	{
		/* check the arguments */
		if (args.length == 0) {
			System.err.println("Usage: java Main [options] classname");
			System.exit(0);
		}

		/* add a phase to transformer pack by call Pack.add */
		Pack jtp = PackManager.v().getPack("jtp");
		jtp.add(new Transform("jtp.livevar", new LiveVariablesInstrumenter()));
		jtp.add(new Transform("jtp.reachdef", new ReachingDefinitionsInstrumenter()));
		jtp.add(new Transform("jtp.verybusy", new VeryBusyExpressionInstrumenter()));

		/* Give control to Soot to process all options */
		soot.Main.main(args);
	}
}
