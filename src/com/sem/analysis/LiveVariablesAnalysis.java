package com.sem.analysis;

import soot.Local;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.scalar.BackwardFlowAnalysis;

import java.util.HashSet;
import java.util.Set;


public class LiveVariablesAnalysis extends BackwardFlowAnalysis<Unit, Set<Local>> {

	public LiveVariablesAnalysis(DirectedGraph<Unit> graph)
	{
		super(graph);
		this.doAnalysis();
	}

	@Override
	protected void merge(Set<Local> in1, Set<Local> in2, Set<Local> out)
	{
		out.clear();
		out.addAll(in1);
		out.addAll(in2);
	}

	@Override
	protected void copy(Set<Local> source, Set<Local> dest)
	{
		dest.clear();
		dest.addAll(source);
	}

	@Override
	protected Set<Local> newInitialFlow()
	{
		return new HashSet<Local>();
	}

	@Override
	protected Set<Local> entryInitialFlow()
	{
		return new HashSet<Local>();
	}

	@Override
	protected void flowThrough(Set<Local> in, Unit d, Set<Local> out) {
		out.clear();
		out.addAll(in);

		//kill
		for (ValueBox b : d.getDefBoxes())
		{
			Value v = b.getValue();
			if (v instanceof Local)
				out.remove(v);
		}
		//gen
		for (ValueBox b : d.getUseBoxes())
		{
			Value v = b.getValue();
			if (v instanceof Local)
			{
				Boolean defLive = false;
				for (ValueBox def : d.getDefBoxes())
				{
					if (def.getValue() instanceof Local && in.contains(def.getValue()))
					{
						defLive = true;
						break;
					}
				}
				if (defLive || d.getDefBoxes().size() == 0)
					out.add((Local) v);
			}
		}
	}
}