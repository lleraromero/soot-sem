package com.sem.analysis;

import soot.Body;
import soot.Local;
import soot.RefLikeType;
import soot.Unit;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ArraySparseSet;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * NonZero Analysis (Forward-May)
 */
public class NonZeroAnalysis extends ForwardBranchedFlowAnalysis<FlowSet> {
	private enum states { Z, NZ, MZ, B }
	private Map<String, states> statusVars;
	public NonZeroAnalysis(UnitGraph g) {
		super(g);

		this.statusVars = new HashMap<String, states>();
		Body b = g.getBody();
		for (Local l : b.getLocals())
		{
			  if (l.getType() instanceof RefLikeType)
				  statusVars.put(l.getName(), states.MZ);
		}

		this.doAnalysis();
	}

	@Override
	protected void merge(FlowSet in1, FlowSet in2, FlowSet out) {
		in1.intersection(in2, out);
	}

	@Override
	protected void copy(FlowSet src, FlowSet dest) {
		src.copy(dest);
	}

	@Override
	protected FlowSet newInitialFlow() {
		return new ArraySparseSet();
	}

	@Override
	protected FlowSet entryInitialFlow() {
		return new ArraySparseSet();
	}

	@Override
	protected void flowThrough(FlowSet srcValue, Unit unit, List<FlowSet> fallOut, List<FlowSet> branchOuts)
	{
	}
}