package com.sem.analysis;

import soot.Body;
import soot.BodyTransformer;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;
import soot.toolkits.graph.ExceptionalUnitGraph;
import soot.toolkits.graph.UnitGraph;

import java.util.Iterator;
import java.util.Map;

public class ReachingDefinitionsInstrumenter extends BodyTransformer
{
	/* internalTransform goes through a method body and
	 * computes IN and OUT of each stmt
	 */
	protected void internalTransform(Body body, String phase, Map options)
	{
		// body's method
		SootMethod method = body.getMethod();

		// debugging
		System.out.println("instrumenting method : " + method.getSignature());

		String signature = method.getSubSignature();

		// re-iterate the body to look for return statement
		if (signature.contains("RD()"))
		{
			UnitGraph g = new ExceptionalUnitGraph(body);
			ReachingDefinitionsAnalysis an = new ReachingDefinitionsAnalysis(g);
			// Iterate over the results
			Iterator<Unit> i = g.iterator();
			while (i.hasNext())
			{
				Stmt u = (Stmt)i.next();
				System.out.println("IN: " + an.getFlowBefore(u).toString()); //IN
				System.out.println(u);
				System.out.println("OUT: " + an.getFlowAfter(u).toString()); //OUT
			}
		}
	}
}
