package com.sem.analysis;

import soot.Local;
import soot.Unit;
import soot.Value;
import soot.ValueBox;
import soot.toolkits.graph.DirectedGraph;
import soot.toolkits.scalar.ArraySparseSet;
import soot.toolkits.scalar.FlowSet;
import soot.toolkits.scalar.ForwardFlowAnalysis;

//Forward-Must

public class ReachingDefinitionsAnalysis extends ForwardFlowAnalysis<Unit, FlowSet>
{
		public ReachingDefinitionsAnalysis(DirectedGraph<Unit> graph)
		{
			super(graph);
			this.doAnalysis();
		}

		@Override
		protected void merge(FlowSet in1, FlowSet in2, FlowSet out)
		{
			in1.intersection(in2, out);
		}

		@Override
		protected void copy(FlowSet src, FlowSet dest)
		{
			src.copy(dest);
		}

		@Override
		protected FlowSet newInitialFlow()
		{
			return new ArraySparseSet();
		}

		@Override
		protected FlowSet entryInitialFlow()
		{
			return new ArraySparseSet();
		}

		@Override
		protected void flowThrough(FlowSet in, Unit node, FlowSet out)
		{
			in.copy(out);
			//kill
			for (ValueBox b : node.getDefBoxes())
			{
				Value v = b.getValue();
				if (v instanceof Local)
					out.remove(v);
			}

			//gen
			for (ValueBox b : node.getUseBoxes())
			{
				Value v = b.getValue();
				if (v instanceof Local)
					out.add(v);
			}
		}
}
